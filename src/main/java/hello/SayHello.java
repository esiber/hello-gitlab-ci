package hello;


public class SayHello {

    public static String hi() {
        return "Hello";
    }

    public static String hi(String who) {
        return "Hello " + who;
    }

}
