package hello;


import org.junit.Assert;
import org.junit.Test;

public class SayHelloTest {

    @Test
    public void hi_should_tell_us_Hello() {
        Assert.assertEquals("Hello", SayHello.hi());
    }

    @Test
    public void hi_Eric_should_tell_us_Hello_Eric() {
        Assert.assertEquals("Hello Eric", SayHello.hi("Eric"));
    }

}
